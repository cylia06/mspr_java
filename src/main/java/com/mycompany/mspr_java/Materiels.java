package com.mycompany.mspr_java;

public class Materiels {
  private String id;
  private String nom;
  private long valeur;
  private int check=0;

  public Materiels(String id, String nom, long valeur) {
    this.id = id;
    this.nom = nom;
    this.valeur = valeur;
  }

  public Materiels(String nom, Long valeur) {
    this.nom = nom;
    this.valeur = valeur;

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getCheck() {
    return check;
  }

  public void setCheck(int check) {
    this.check = check;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public long getValeur() {
    return valeur;
  }

  public void setValeur(Long valeur) {
    this.valeur = valeur;
  }
}
