package com.mycompany.mspr_java;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;
import com.amazonaws.util.IOUtils;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;


public class Aws {

    public static Agents CompareFaces(ArrayList<Agents> agents) throws Exception {
        Float similarityThreshold = 70F;
        String sourceImage = "Image/photoWebcam.png";
        ByteBuffer sourceImageBytes=null;
        ByteBuffer targetImageBytes=null;

        BasicAWSCredentials awsCreds = new BasicAWSCredentials("access_key_id", "secret_key_id");
        AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build();

        //Load source and target images and create input parameters
        try (InputStream inputStream = new FileInputStream(new File(sourceImage))) {
            sourceImageBytes = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
        }
        catch(Exception e)
        {
            System.out.println("Failed to load source image " + sourceImage);
            System.exit(1);
        }
        for (Agents agent: agents) {
            URL url = null;
            URLConnection uc;
            try {
                url = new URL(agent.getPhoto());
                uc = url.openConnection();
                uc.addRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");  // obliger de passé par un user agent sinon ca retourne une 403
                uc.connect();
                targetImageBytes = ByteBuffer.wrap(IOUtils.toByteArray(uc.getInputStream()));
            } catch (Exception e) {
                System.out.println("Failed to load target images: " + agent.getPhoto());
                System.exit(1);
            }

            Image source = new Image()
                    .withBytes(sourceImageBytes);
            Image target = new Image()
                    .withBytes(targetImageBytes);

            CompareFacesRequest request = new CompareFacesRequest()
                    .withSourceImage(source)
                    .withTargetImage(target)
                    .withSimilarityThreshold(similarityThreshold);

            // Call operation
            CompareFacesResult compareFacesResult = rekognitionClient.compareFaces(request);

            // Display results
            List<CompareFacesMatch> faceDetails = compareFacesResult.getFaceMatches();
            for (CompareFacesMatch match : faceDetails) {
                ComparedFace face = match.getFace();
                BoundingBox position = face.getBoundingBox();
                if (face.getConfidence() > 70) {
                    return agent;
                }
            }
        }
        return null;
    }
}
