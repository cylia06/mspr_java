package com.mycompany.mspr_java;

import org.apache.log4j.BasicConfigurator;

import javax.swing.*;
import java.util.ArrayList;

public class Main {

  private static JFrame f = new JFrame("Stockage");

  public static void main(String[] args) {

    BasicConfigurator.configure();
    f.setSize(850, 750);
    Firebase firebase = new Firebase();
    ArrayList<Agents> agents;
    firebase.ListeMateriels();
    agents= firebase.ListeAgents();
    Stock s =new Stock(firebase);
    // Agents a = firebase.getAgent("RDzROc2iOMWyalSdw2mB"); // Permet de testé directement la vue stock sans s'authentifier

    f.getContentPane().add(new ConnexionVue(agents)); // Commenter la ligne pour accedé a la vue stock sans s'authentifier
    // f.getContentPane().add(new FormVueStock(a)); // Permet d'accedé a la vue stock sans s'authentifier
    f.setVisible(true);

  }

  public static void changePanel(JPanel remove, JPanel add) {
    f.getContentPane().remove(remove);
    f.getContentPane().add(add);
    f.invalidate();
    f.validate();
  }

}
