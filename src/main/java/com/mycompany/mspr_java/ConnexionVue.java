/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mspr_java;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;

import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;

public class ConnexionVue extends javax.swing.JPanel {

  private ArrayList<Agents> agents;
  private JButton jButton1;
  private JPanel jPanel1;

  public ConnexionVue(ArrayList<Agents> agents) {
    init();
    this.agents = agents;
  }

  public void init() {
    jButton1 = new javax.swing.JButton();
    jPanel1 = new javax.swing.JPanel();

    Webcam webcam = Webcam.getDefault();
    webcam.setViewSize(WebcamResolution.VGA.getSize());

    WebcamPanel panel = new WebcamPanel(webcam);
    panel.setFPSDisplayed(true);
    panel.setDisplayDebugInfo(true);
    panel.setImageSizeDisplayed(true);
    panel.setMirrored(true);
    jButton1.setText("S'identifier");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton1ActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 100, Short.MAX_VALUE)
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 100, Short.MAX_VALUE)
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
            .addComponent(jButton1)
            .addGap(42, 42, 42))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(64, 64, 64)
                    .addComponent(jButton1))
                .addGroup(layout.createSequentialGroup()
                    .addGap(73, 73, 73)
                    .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(499, Short.MAX_VALUE))
    );


    jPanel1.add(panel);
    panel.revalidate(); // invoke the layout manager
    panel.repaint();
  }

  public void takePhoto() {
    Webcam webcam = Webcam.getDefault();
    webcam.open();

    // get image
    BufferedImage image = webcam.getImage();
    webcam.close();
    try {
      ImageIO.write(image, "PNG", new File("Image/photoWebcam.png"));
    } catch (Exception IOException) {
      System.out.println(IOException);
    }
  }

  private void jButton1ActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    this.takePhoto();
    try {
      Agents agent = Aws.CompareFaces(this.agents);
      if (agent != null) {
        Main.changePanel(this, new FormVueStock(agent));
      }
    } catch (Exception Exception) {
      System.out.println(Exception);
    }

  }

}
