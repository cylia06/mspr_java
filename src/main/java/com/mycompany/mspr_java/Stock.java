package com.mycompany.mspr_java;

import java.util.ArrayList;

public  class Stock {

  private static ArrayList<Materiels> materiels=new ArrayList<Materiels>();
  private static Firebase firebase;

  public Stock(Firebase firebase){
    this.firebase = firebase;
    materiels=firebase.ListeMateriels();
  }

  public static ArrayList<Materiels> getMateriels() {
    return materiels;
  }

  public static void setMateriels(ArrayList<Materiels> materiels) {
    Stock.materiels = materiels;
  }

  public static void deposer (String m)
  {
    for (Materiels mat :materiels) {
      if (mat.getNom().equals(m)) {
        mat.setValeur(mat.getValeur()+1);
        mat.setCheck(1);
      }
    }
  }

  public static void retirer (String m)
  {
    for (Materiels mat :materiels) {
      if (mat.getNom().equals(m)) {
        mat.setValeur(mat.getValeur() - 1);
        mat.setCheck(-1);
      }
    }
  }

  public static void valider(String agentId)
  {
    firebase.updateMateriel(materiels, agentId);
  }

}
