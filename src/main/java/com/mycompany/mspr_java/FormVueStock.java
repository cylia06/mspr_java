/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.mycompany.mspr_java;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

public class FormVueStock extends javax.swing.JPanel {

  private ArrayList<Materiels> materiels;
  private ArrayList<JCheckBox> listCheckBox = new ArrayList<JCheckBox>();
  private Agents a;
  private JButton jButton1;
  private JLabel jLabel2;

  public FormVueStock(Agents a) {
    this.a = a;
    this.materiels=Stock.getMateriels();
    remplirCheck_box();
    init();
  }

  private void init() {

    jButton1 = new JButton();
    jLabel2 = new JLabel();

    jButton1.setText("Identification");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });

    try {
      jLabel2 = new JLabel();
      URL url = null;
      BufferedImage urlImage = null;
      URLConnection uc;
      try {
        url = new URL(this.a.getPhoto());
        uc = url.openConnection();
        uc.addRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");  // obliger de passé par un user agent sinon ca retourne une 403
        uc.connect();
        urlImage = ImageIO.read(uc.getInputStream());

      } catch (MalformedURLException ex) {
        System.out.println("Malformed URL");
      } catch (IOException iox) {
        System.out.println("Can not load file");
          iox.printStackTrace();
      }
      Image formatImage = urlImage.getScaledInstance(100, 150, Image.SCALE_DEFAULT); // resize de l'image
      ImageIcon iconPhoto = new ImageIcon(formatImage);
      jLabel2.setIcon(iconPhoto); // NOI18N
    } catch (Exception exception) {
      System.out.println(exception);
    }

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);

    this.setLayout(layout);
    GroupLayout.Group horizGroup = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
    for (JCheckBox checkBox : listCheckBox) {

      horizGroup.addComponent(checkBox);
    }

    layout.setHorizontalGroup(
    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(jButton1)
            .addGap(500, 500, 500)
            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addGroup(layout.createSequentialGroup()
            .addGap(128, 128, 128)

            .addGroup(horizGroup)))
      .addContainerGap(92, Short.MAX_VALUE))
    );
    GroupLayout.Group verticalGroup = layout.createSequentialGroup()
      .addContainerGap();
    verticalGroup.addComponent(jButton1);
    verticalGroup.addGap(35, 35, 35);
    verticalGroup.addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE);
    verticalGroup.addGap(30, 30, 30);

    for (JCheckBox checkbox : listCheckBox) {
      verticalGroup.addComponent(checkbox);
      verticalGroup.addGap(10, 10, 10);
    }
    verticalGroup.addGap(71,71, 71);

    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(verticalGroup)
    );

  }

  private void jCheckBoxActionPerformed(java.awt.event.ActionEvent evt, String mat, Boolean selected) {
    // TODO add your handling code here:
    if (selected) {
      this.a.priseMateriel(mat);
    } else {
      this.a.deposeMateriel(mat);
    }

  }

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
    // TODO add your handling code here:
    this.a.valider();
    Main.changePanel(this, new ConnexionVue(new Firebase().ListeAgents()));
  }

  private void remplirCheck_box() {
    for (Materiels m  : this.materiels) {
      String nom_checkBox= m.getNom();
      JCheckBox nomCheckBox =new JCheckBox(nom_checkBox);
      if (m.getValeur()== 0) {
        nomCheckBox.setEnabled(false);
      }
      if(verifListMatAgent(this.a.getMateriels(), nom_checkBox)) {
        nomCheckBox.setSelected(true);
      }

      nomCheckBox.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
          jCheckBoxActionPerformed(evt, nom_checkBox, nomCheckBox.isSelected());
        }
      });
      this.listCheckBox.add(nomCheckBox);

    }
  }

  @Override
  public void print(Graphics g) {
    super.print(g); //To change body of generated methods, choose Tools | Templates.
  }

  private boolean verifListMatAgent(ArrayList<Materiels> list, String mat) {
    for (Materiels m : list ) {
      if (m.getNom().equals(mat)) {
        return true;
      }
    }
    return false;
  }

}
