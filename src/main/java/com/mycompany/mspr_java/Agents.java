package com.mycompany.mspr_java;

import java.util.ArrayList;
import java.util.Date;


public class Agents {

  private String id;
  private String nom;
  private String prenom;
  private String numenocarte;
  private Date datenaissance;
  private Date dateentree;
  private String adresse;
  private String photo;
  private ArrayList<Materiels> materiels =new ArrayList<Materiels>();


  //////// constructeur //////////////////////////////////////////

  public Agents(){}
  public Agents(String id, String nom, String prenom, String numenocarte, Date datenaissance, Date dateentree,
  String adresse, String photo, ArrayList<Materiels> materiels) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.numenocarte = numenocarte;
    this.datenaissance = datenaissance;
    this.dateentree = dateentree;
    this.adresse = adresse;
    this.photo = photo;
    this.materiels = materiels;
  }
  //////// getter and setter //////////////////////////////////////////////////////////

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getNumenocarte() {
    return numenocarte;
  }

  public void setNumenocarte(String numenocarte) {
    this.numenocarte = numenocarte;
  }

  public String getAdresse() {
    return adresse;
  }

  public void setAdresse(String adresse) {
    this.adresse = adresse;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }


  public ArrayList<Materiels> getMateriels() {
    return materiels;
  }

  public void setMateriels(ArrayList<Materiels> materiels) {
    this.materiels = materiels;
  }

  ///////////////////////////////////////////////////////////////////////////////


  public void  priseMateriel (String nom ) {
    this.materiels.add(new Materiels(nom, (long) 1));
    Stock.retirer(nom);
  }

  public void deposeMateriel (String nom) {
    for (Materiels m:materiels) {
      if (m.getNom().equals(nom)) {
        this.materiels.remove(m);
        Stock.deposer(nom);
      }
    }

  }

  public void valider() {
    Stock.valider(this.id);
  }

}
