
package com.mycompany.mspr_java;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;

import com.google.cloud.firestore.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.util.*;


public class Firebase {
    private Firestore db;

    public Firebase() {
      try {
        FileInputStream serviceAccount = new FileInputStream("Credentials.json");
        GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
        FirebaseOptions options =new FirebaseOptions.Builder()
          .setCredentials(credentials)
          .build();

        FirebaseApp.initializeApp(options);
        this.db = FirestoreClient.getFirestore();
      } catch (Exception fileNotFound) {
        System.out.println(fileNotFound);
      }

    }

    public Agents getAgent( String id) {
      try {
        DocumentReference docRef = db.collection("agents").document(id);
        // asynchronously retrieve the document
        ApiFuture<DocumentSnapshot> future = docRef.get();
        DocumentSnapshot document = future.get();
        if (document.exists()) {
          ArrayList<Materiels> materiels= new ArrayList<Materiels>();
          List<String> mat = (List<String>) document.get("materiel");
          for (String m:mat) {
            materiels.add(new Materiels(m,(long)1));
          }
          Date S1 = (Date)document.get("dateEntr");
          Date S2= (Date)document.get("dateNaissance");
          Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

          Agents a =new Agents(document.getId(), document.getString("nom"), document.getString("prenom"),
            document.getString("numIdentite"), S2, S1, document.getString("adresse"), document.getString("image"), materiels);
          return a;

        } else {
          System.out.println("No such document!");
          return null;
        }
      } catch (Exception DataNotFound) {
        System.out.println(DataNotFound);
        return null;
      }
    }

    public void updateMateriel(ArrayList<Materiels> list, String agentId) {
      for (Materiels m:list) {
        if (m.getCheck() != 0) {
          try {
            final DocumentReference docRefAgent = db.collection("agents").document(agentId);
            if (m.getCheck() == -1) {
              docRefAgent.update("materiel",FieldValue.arrayUnion(m.getNom())).get();
            } else {
              docRefAgent.update("materiel",FieldValue.arrayRemove(m.getNom())).get();
            }
            final DocumentReference docRefMateriel = db.collection("materiels").document(m.getId());
            docRefMateriel.update("valeur",m.getValeur()).get();

          } catch ( Exception FirebaseFirestoreException) {
            System.out.println(FirebaseFirestoreException);
          }
        }
      }
    }

    public ArrayList<Agents> ListeAgents() {
      ArrayList<Agents> agents= new ArrayList<Agents>();
      try {
        ApiFuture<QuerySnapshot> query = this.db.collection("agents").get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        for (QueryDocumentSnapshot document : documents) {

          ArrayList<Materiels> materiel= new ArrayList<Materiels>();
          List<String> mat = (List<String>) document.get("materiel");
          for (String m:mat) {
            materiel.add(new Materiels(m,(long)1));
          }

          Date S1 = (Date)document.get("dateEntr");
          Date S2 = (Date)document.get("dateNaissance");
          Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

          agents.add(new Agents(document.getId(), document.getString("nom"), document.getString("prenom"),
          document.getString("numIdentite"), S2, S1, document.getString("adresse"), document.getString("image"), materiel));

          Map<String, Object> map = document.getData();
        }
      } catch (Exception FirebaseFirestoreException) {
        System.out.println(FirebaseFirestoreException);
      }

      return agents;
    }

    public ArrayList<Materiels>  ListeMateriels() {
      ArrayList<Materiels> materiels= new ArrayList<Materiels>();
      try {
        ApiFuture<QuerySnapshot> query = this.db.collection("materiels").get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        for (QueryDocumentSnapshot document : documents) {
          materiels.add(new Materiels(document.getId(),document.getString("nom"),document.getLong("valeur")));
          Map<String, Object> map = document.getData();
        }

      } catch (Exception FirebaseFirestoreException) {
        System.out.println(FirebaseFirestoreException);
      }

      return materiels;
    }

}
